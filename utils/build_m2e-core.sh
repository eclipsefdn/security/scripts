#!/bin/bash

### Firstly please beaware of having the right mvn version
### mvn minimum version 3.8.6 
### Already cloned repo m2e-core as well as recommended to have a PR already pushed
### TIPs: 
### 1. undoing all changes applied for this script. Please execute: mvn clean && git checkout master && git reset --hard origin/master
### 2. Ubuntu to configure mvn 3.8.6, 
### 	sudo update-alternatives --install /usr/bin/mvn mvn /opt/maven/apache-maven-3.8.6/bin/mvn 120
###		sudo update-alternatives --config mvn

get_pr_id(){
	local PR_ARG=$1
	OIFS=$IFS
	IFS='/'
	ARR_PR_ARG_TMP=(${PR_ARG})
	IFS='-'
	ARR_PR_ARG=(${ARR_PR_ARG_TMP})
	echo ${ARR_PR_ARG[1]}

}


main(){


	export GIT_MERGE_AUTOEDIT=no
	PULL_ID=$1

	### Checking PR-ID format, for instance PR-1361 with ID 1361 (where SBOM was added)
	re="[0-9]{1,}"
	[[ ! $PULL_ID =~ $re ]] && echo "ERROR!!! wrong PULL REQUEST ID format: ${PULL_ID}. Only numbers are allowed" && return 1

	### Checking PWD is a m2e-core repo
	result=$(git remote -v 2>/dev/null)
	re_repo=m2e-core
	[[ ! $result =~  $re_repo ]] && echo "ERROR!!! current PWD: ${PWD} is not repo: ${re_repo}" && return 1 

	### Fetching master and PULL_ID
	git fetch --no-tags --force --progress -- https://github.com/eclipse-m2e/m2e-core.git +refs/pull/${PULL_ID}/head:refs/remotes/origin/PR-${PULL_ID} +refs/heads/master:refs/remotes/origin/master
	
	### Loading trusted files from base branch master at ca3fbcd35e40eb7feabdddec81120f06f8bb734c
	git merge ca3fbcd35e40eb7feabdddec81120f06f8bb734c
	COMMIT_SHA=$(git rev-parse HEAD^{commit})
	git checkout -f ${COMMIT_SHA}
	
	### Updating submodule
	git submodule update --init --recursive --remote

	### Building locally
	mvn clean verify -Dtycho.p2.baselineMode=failCommon --batch-mode -Pits
	
	
	unset GIT_MERGE_AUTOEDIT
}

main "$@"
