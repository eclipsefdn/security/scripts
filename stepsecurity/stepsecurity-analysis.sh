#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'
STEPSECURITY_API=https://app.stepsecurity.io/securerepo?repo 


main(){
  xdg-open ${STEPSECURITY_API}=${@} > /dev/null 2>&1
}

main ${@}