#!/usr/bin/env bash

set -euo pipefail

jq -r '.[]|select(.state!="Archived")|select(isempty(.github_repo)|not)|select([.github_repos[].url]|contains(["https://github.com/eclipse/"]))|([.github_repos[]|select(.url|contains("https://github.com/eclipse/"))]|length|tostring) + ";" + .project_id' | sort -n