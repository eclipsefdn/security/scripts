#!/usr/bin/env bash

set -euo pipefail

RND_STR=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 13 ; echo '')
PULLS=""
URL="https://projects.eclipse.org/api/projects?pagesize=100&${RND_STR}"
while [[ -n "${URL:-}" ]]; do
  >&2 echo "Fetching $URL..."
  RESPONSE=$(curl -i -sSLf "${URL}")
  HEADERS="$(sed '/^\r$/q' <<<"${RESPONSE}")"
  URL="$(sed -n -E 's/link:.*<(.*?)>; rel="next".*/\1/ip' <<<"${HEADERS}")"
  PULLS="${PULLS} $(sed '1,/^\r$/d' <<<"${RESPONSE}")"
done

echo "${PULLS}"