#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

list_active_membership () {
  local ORGS ORG
  ORGS=$(gh api user/memberships/orgs --paginate | jq -r '.[] | select(.state | contains("active")) | .organization.login')
  echo "Member of the following GitHub organizations:"
  for ORG in ${ORGS}
  do
    echo "  ${ORG}"
  done
}

list_pending_membership () {
  local ORGS ORG
  ORGS=$(gh api user/memberships/orgs --paginate | jq -r '.[] | select(.state | contains("pending")) | .organization.login')
  echo "Invitation to join the following GitHub organizations:"
  for ORG in ${ORGS}
  do
    echo "  ${ORG}"
  done
}

join_pending_membership () {
  local ORGS
  ORGS=$(gh api user/memberships/orgs --paginate | jq -r '.[] | select(.state | contains("pending")) | .organization.login')
  for ORG in ${ORGS}
  do
    echo "Joining org ${ORG}"
    gh api "user/memberships/orgs/${ORG}" --method PATCH --silent -f state="active"
  done
}

usage() {
  local USAGE
  USAGE="
Usage: $(basename "${0}") [OPTIONS]

Options:
  -a      display active organization memberships
  -p      display pending organization memberships
  -j      accept pending organization invitations
  -h      show this help

"
  echo "$USAGE"
}

ACTION=""

while getopts "apj" opt; do
    case "${opt}" in
        a)
            ACTION="list-active"
            ;;
        p)
            ACTION="list-pending"
            ;;
        j)
            ACTION="join-pending"
            ;;
        *)
            usage
            exit 0
            ;;
    esac
done

if [ -z "$ACTION" ]; then
  usage
  exit 1
fi

shift $((OPTIND-1))

if [[ -z "${GH_TOKEN:-}" ]]; then
  read -p "Enter your GitHub token: " -r GH_TOKEN
  export GH_TOKEN
fi

command -v gh >/dev/null 2>&1 || { 
  echo >&2 "This script requires the GitHub CLI tool (gh), but it's not installed."
  echo >&2 "Please download and install it from https://github.com/cli/cli."
  echo >&2 "Aborting."
  exit 1
}

case $ACTION in
  "list-active") list_active_membership ;;
  "list-pending") list_pending_membership ;;
  "join-pending") join_pending_membership ;;
   *) exit 1 ;;
esac

