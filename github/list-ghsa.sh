#!/usr/bin/env bash

# Before calling this script, you need to set the following environment variables:
# $ export GH_TOKEN="ghp_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

# requires:
# * jq (https://stedolan.github.io/jq/)
# * gh (https://cli.github.com/)
# * jsonnet (https://jsonnet.org/)

set -euo pipefail
IFS=$'\n\t'

{
  for org in $(gh api --cache 3h --paginate /user/orgs | jq -r '.[].login'); do
    if gh api --cache 3h --paginate "/orgs/${org}/security-advisories" >/dev/null 2>&1; then
      sa="$(gh api --cache 3h --paginate "/orgs/${org}/security-advisories")"
      if [[ $(jq -e 'length' <<<"${sa}") -gt 0 ]] ; then
        echo "${sa}+"
      else
        >&2 echo "${org}: no security advisories"
      fi
    else
      >&2 echo "${org}: no permission to get security advisories"
    fi
  done
  echo '[]'
} | jsonnet - | jq