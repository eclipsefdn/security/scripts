#!/usr/bin/env bash

# retrieve the members of an organization and their details in a CSV file
# Usage: ./get_org_members_infos.sh <org_name> [project_name]
# useful for importing the data in a spreadsheet to manage the members of an organization

set -euo pipefail

IFS=$'\n\t'

ORG_NAME="${1}"
PROJECT_NAME="${2:-}"

rm -rf "${ORG_NAME}" 
mkdir -p "${ORG_NAME}"
pushd "${ORG_NAME}" >/dev/null || exit 1

if ! command -v jq >/dev/null; then
  echo "jq is not installed"
  exit 1
fi

if ! command -v gh >/dev/null; then
  echo "gh is not installed"
  exit 1
fi

if ! command -v bw >/dev/null; then
  echo "bw is not installed"
  exit 1
fi

if ! command -v pass >/dev/null; then
  echo "pass is not installed"
  exit 1
fi

# if BW_SESSION is not set, exit 1
if [[ -z "${BW_SESSION}" ]]; then
  echo "BW_SESSION is not set"
  exit 1
fi

GH_TOKEN="$(bw get item "39adacc9-2b51-41a9-a27e-ac7c00eea6a5" | jq -r '.fields[]|select(.name=="api_token_admin").value')"
export GH_TOKEN

request_access_token() {
  local client_id client_secret
  client_id="$(pass api.eclipse.org/client_id)"
  client_secret="$(pass api.eclipse.org/client_secret)"
  curl -sSLf --request POST \
    --url 'https://accounts.eclipse.org/oauth2/token' \
    --header 'content-type: application/x-www-form-urlencoded' \
    --data 'grant_type=client_credentials' \
    --data "client_id=${client_id}" \
    --data "client_secret=${client_secret}" \
    --data 'scope=eclipsefdn_view_all_profiles' | jq -r '.access_token'
}
ECLIPSE_ACCESS_TOKEN="$(request_access_token)"

_gh() {
  gh api -X GET --cache 12h --paginate "$@"
}
export _gh

get_users_category() {
  local category="${1}"
  _gh "orgs/${ORG_NAME}/${category}" > "${category}.json"
  jq <"${category}.json" | jq -r '.[].login' | sort -u > "${category}.txt"

  _gh -f "filter=2fa_disabled" "orgs/${ORG_NAME}/${category}" > "${category}_2fa_disabled.json"
  jq <"${category}_2fa_disabled.json" | jq -r '.[].login' | sort -u > "${category}_2fa_disabled.txt"

  comm -23 "${category}.txt" "${category}_2fa_disabled.txt" > "${category}_2fa_enabled.txt"
}

gh_id_in_json_file() {
  local github_id="${1}"
  local json_file="${2}"
  local trueString="${3:-true}"
  local falseString="${4:-false}"
  if jq -e --arg m "${github_id}" '.[] | select(.login==$m)' <"${json_file}" >/dev/null; then 
    echo "${trueString}"; 
  else 
    echo "${falseString}";
  fi
}
export gh_id_in_json_file

gh_id_in_txt_file() {
  local github_id="${1}"
  local txt_file="${2}"
  local trueString="${3:-true}"
  local falseString="${4:-false}"
  if grep -q "${github_id}" "${txt_file}"; then 
    echo "${trueString}"; 
  else 
    echo "${falseString}";
  fi
}

# Function to get GitHub user details
get_github_details() {
  local gh_id="${1}"
  local gh_details
  gh_details=$(_gh "users/${gh_id}")
  echo "$(jq -r '.name' <<<"${gh_details}"),$(jq -r '.email' <<<"${gh_details}")"
}

# Function to get Eclipse user details
get_eclipse_details() {
  local gh_id="${1}"
  local eclipse_gh_profile
  eclipse_gh_profile=$(curl -sSL -H "Authorization: Bearer ${ECLIPSE_ACCESS_TOKEN}" "https://api.eclipse.org/github/profile/${gh_id}")
  echo "$(jq -r '.name' <<<"${eclipse_gh_profile}"),$(jq -r '.full_name' <<<"${eclipse_gh_profile}"),$(jq -r '.mail' <<<"${eclipse_gh_profile}")"
}

# Function to check if user is a project lead, committer, or contributor
check_user_role() {
  local eclipse_id=$1
  local role=$2
  jq -r --arg eclipse_id "${eclipse_id}" '([.'"${role}"'[] | select(.username == $eclipse_id)] | length) == 1' <"project.json"
}

# function that joins 2 strings with a separator (comma by default) that handles values "null" and "" as empty
# function should answer the following cases:
  # "null","null" -> ""
  # "null","foo" -> "foo"
  # "foo","null" -> "foo"
  # "foo","bar" -> "foo,bar"
  # "foo","foo" -> "foo"
  # "","" -> ""
  # "","foo" -> "foo"
  # "foo","" -> "foo"
  # "","null" -> ""
  # "null","" -> ""
join() {
  local separator="${1:-,}"
  local first="${2:-}"
  local second="${3:-}"
  
  if [[ "${first}" == "null" ]]; then
    first=""
  fi
  if [[ "${second}" == "null" ]]; then
    second=""
  fi

  if [[ "${first}" == "${second}" ]]; then
    echo "${first}"
  elif [[ -z "${first}" ]]; then
    echo "${second}"
  elif [[ -z "${second}" ]]; then
    echo "${first}"
  else
    echo "${first}${separator}${second}"
  fi
}
export join

get_users_category "members"
get_users_category "outside_collaborators"

_gh "orgs/${ORG_NAME}/public_members" > "public_members.json"
jq <"public_members.json" | jq -r '.[].login' | sort -u > "public_members.txt"

_gh -f "role=admin" "orgs/${ORG_NAME}/members" > "admins.json"
comm -23 "members.txt" "public_members.txt" > "private_members.txt"

mkdir users
for t in $(_gh "orgs/${ORG_NAME}/teams" | jq -r '.[]|.slug'); do 
  mkdir -p "teams/${t}" 
  _gh "orgs/${ORG_NAME}/teams/${t}/members" > "teams/${t}/members.json"
  # for each members, add the team it's part of
  jq -r '.[].login' <"teams/${t}/members.json" | sort -u | while read -r github_id; do
    echo "${t}" >> "users/${github_id}.txt"
  done
done

if [[ -n "${PROJECT_NAME}" ]]; then
  curl -sSL "https://projects.eclipse.org/api/projects/${PROJECT_NAME}" | jq '.[0]' > "project.json"
fi

#_gh "orgs/${ORG_NAME}/invitations" > invitations.json
#_gh "orgs/${ORG_NAME}/failed_invitations" > failed_invitations.json

echo "gh_id;eclipse_id;name;email;mfa_enabled;gh_org_role;gh_org_membership_visibility;is_outside_collaborator;is_eclipse_project_lead;is_eclipse_committer;is_eclipse_contributor;is_eclipse_managed;gh_steams" > members.csv

# shellcheck disable=SC2013
for gh_id in $(cat outside_collaborators.txt members.txt | sort); do
  IFS=',' read -r gh_name gh_email <<< "$(get_github_details "${gh_id}")"
  
  eclipse_id="null"
  eclipse_name="null"
  eclipse_email="null"
  is_eclipse_project_lead="false"
  is_eclipse_committer="false"
  is_eclipse_contributor="false"
  if [[ $(curl -sL -o /dev/null -w "%{http_code}" -I "https://api.eclipse.org/github/profile/${gh_id}") == 200 ]]; then
    IFS=',' read -r eclipse_id eclipse_name eclipse_email <<< "$(get_eclipse_details "${gh_id}")"
    
    if [[ -n "${PROJECT_NAME}" ]]; then
      is_eclipse_project_lead=$(check_user_role "${eclipse_id}" "project_leads")
      is_eclipse_committer=$(check_user_role "${eclipse_id}" "committers")
      is_eclipse_contributor=$(check_user_role "${eclipse_id}" "contributors")
    fi
  fi

  gh_org_role=$(gh_id_in_json_file "${gh_id}" "admins.json" "admin" "member")
  gh_org_membership_visibility=$(gh_id_in_txt_file "${gh_id}" "public_members.txt" "public" "private")
  is_outside_collaborator="$(gh_id_in_txt_file "${gh_id}" "outside_collaborators.txt")"
  
  # has_invite="$(gh_id_in_json_file "${gh_id}" "invitations.json")"
  # has_failed_invite="$(gh_id_in_json_file "${gh_id}" "failed_invitations.json")"
  
  # if they are outside collaborator, check for 2FA in the outside_collaborators.json file 
  if [[ "${is_outside_collaborator}" == "true" ]]; then
    mfa_enabled="$(gh_id_in_txt_file "${gh_id}" "outside_collaborators_2fa_enabled.txt")"
  else
    mfa_enabled="$(gh_id_in_txt_file "${gh_id}" "members_2fa_enabled.txt")"
  fi

  if [ -f "users/${gh_id}.txt" ]; then
    gh_teams="$(<"users/${gh_id}.txt" tr '\n' ',' | sed 's/,$//')"
  else
    gh_teams=""
  fi

  is_eclipse_managed="FALSE"
  if [[ "${is_eclipse_project_lead}" == "true" ]] || [[ "${is_eclipse_committer}" == "true" ]] || [[ "${is_eclipse_contributor}" == "true" ]]; then
    is_eclipse_managed="true"
  fi

  echo "${gh_id};${eclipse_id};$(join "${gh_name}" "${eclipse_name}");$(join "${gh_email}" "${eclipse_email}");${mfa_enabled};${gh_org_role};${gh_org_membership_visibility};${is_outside_collaborator};${is_eclipse_project_lead};${is_eclipse_committer};${is_eclipse_contributor};${is_eclipse_managed};${gh_teams}" | tee -a members.csv
done

popd >/dev/null || exit 1
